<?php

require_once("../vendor/autoload.php");

use \Trial\App\Model\Visitor;

$env = Dotenv\Dotenv::createImmutable('../');
$env->load();

$ip_address = $_SERVER['REMOTE_ADDR'];
$user_agent = $_SERVER['HTTP_USER_AGENT'];
$page_url   = $_SERVER['HTTP_REFERER'];

if ($ip_address && $user_agent && $page_url) {
    $visitor = new Visitor($ip_address, $user_agent, $page_url);
    $visitor->visit();
    echo file_get_contents('images/test.png');
} else {
    echo file_get_contents('images/test.png');
}

?>