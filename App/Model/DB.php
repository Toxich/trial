<?php

namespace Trial\App\Model;

class DB
{
    private static $_instance;

    private function __construct()
    {
        self::$_instance = new \PDO(
            'mysql:host=' . $_ENV['DB_HOST'] . ';dbname=' . $_ENV['DB_NAME'],
            $_ENV['DB_USER'],
            $_ENV['DB_PASS'],
            [
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
            ]
        );
    }

    public static function getInstance()
    {
        if (!self::$_instance) {
            new self();
        }
        return self::$_instance;
    }


}