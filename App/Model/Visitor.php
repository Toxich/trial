<?php

namespace Trial\App\Model;

use Trial\App\Model\DB;


class Visitor
{
    protected $id;
    protected $ip_address;
    protected $user_agent;
    protected $page_url;
    protected $views_count;

    public function __construct($ip_address = null, $user_agent = null, $page_url = null)
    {
        $this->ip_address = $ip_address;
        $this->user_agent = $user_agent;
        $this->page_url   = $page_url;
    }

    public function visit()
    {
        $conn = DB::getInstance();
        $stmt = $conn->prepare(
            "SELECT id, views_count FROM `visitors` WHERE `ip_address` = :ip_address AND `page_url` = :page_url AND `user_agent` = :user_agent "
        );
        $stmt->execute(
            ['ip_address' => $this->ip_address, 'page_url' => $this->page_url, 'user_agent' => $this->user_agent]
        );
        $data = $stmt->fetch();
        if ($data) {
            $this->views_count = $data['views_count'] + 1;
            $stmt = $conn->prepare(
                "UPDATE visitors SET views_count = :views_count WHERE ip_address = :ip_address AND user_agent = :user_agent AND page_url = :page_url"
            );
            $stmt->execute(
                [
                    'views_count' => $this->views_count,
                    'ip_address'  => $this->ip_address,
                    'user_agent'  => $this->user_agent,
                    'page_url'    => $this->page_url
                ]
            );

            return $data['id'];
        } else {
            $stmt = $conn->prepare(
                "INSERT INTO visitors(`ip_address`, `user_agent`, `views_count`, `page_url`) VALUES (:ip_address, :user_agent, :views_count, :page_url )"
            );
            $stmt->execute(
                [
                    'ip_address'  => $this->ip_address,
                    'user_agent'  => $this->user_agent,
                    'views_count' => 1,
                    'page_url'    => $this->page_url
                ]
            );
            $this->id = $conn->lastInsertId();

            return $this->id;
        }
    }


}